package stage3.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import stage3.pageObjects.LoginPage;
import stage3.pageObjects.MainPage;

public class LoginTest {

    @Test
    void shouldDisplayAuthenticationErrorWhenProvidedIncorrectCredentials() {
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver,5);

        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        loginPage.loginUser("fakeuser@op.pl", "fakepassword");
        Assertions.assertTrue(loginPage.isFailedAuthenticationAlertDisplayed(), "failed authentication message should be displayed");
    }

    @Test
    void shouldDisplayPasswordRequiredMessageWhenNoPasswordProvided() {
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver,5);

        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        loginPage.loginUser("fakeuser@op.pl", "");
        Assertions.assertTrue(loginPage.isPasswordRequiredAlertDisplayed(), "password required message should be displayed");
    }
}