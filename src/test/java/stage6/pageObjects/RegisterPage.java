package stage6.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage extends BasePage{

    private WebDriver driver;
    private WebDriverWait wait;

    public RegisterPage(WebDriver webDriver, WebDriverWait webDriverWait) {
        super(webDriver, webDriverWait);
        this.driver = webDriver;
        this.wait = webDriverWait;
    }

    @FindBy(css = "h1.page-heading")
    private WebElement pageHeader;

    @FindBy(id = "customer_firstname")
    private WebElement firstNameInput;

    @FindBy(id = "customer_lastname")
    private WebElement lastNameInput;

    @FindBy(id = "email")
    private WebElement emailInput;

    @FindBy(id = "passwd")
    private WebElement passwordInput;

    public boolean isDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(firstNameInput));
        System.out.println(pageHeader.getText());
        return pageHeader.getText().contains("CREATE AN ACCOUNT");
    }
}
