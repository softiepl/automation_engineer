package stage4.tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import stage4.pageObjects.LoginPage;
import stage4.pageObjects.MainPage;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void init() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver,5);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterEach
    void cleanUp() {
        driver.close();
    }

    @Test
    void shouldDisplayAuthenticationErrorWhenProvidedIncorrectCredentials() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        loginPage.loginUser("fakeuser@op.pl", "fakepassword");
        Assertions.assertTrue(loginPage.isFailedAuthenticationAlertDisplayed(), "failed authentication message should be displayed");
    }

    @Test
    void shouldDisplayPasswordRequiredMessageWhenNoPasswordProvided() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        loginPage.loginUser("fakeuser@op.pl", "");
        Assertions.assertTrue(loginPage.isPasswordRequiredAlertDisplayed(), "password required message should be displayed");
    }
}