package stage5.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import stage5.pageObjects.LoginPage;
import stage5.pageObjects.MainPage;

public class LoginTest extends BaseTest {

    @Test
    void shouldDisplayAuthenticationErrorWhenProvidedIncorrectCredentials() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        loginPage.loginUser("fakeuser@op.pl", "fakepassword");
        Assertions.assertTrue(loginPage.isFailedAuthenticationAlertDisplayed(), "failed authentication message should be displayed");
    }

    @Test
    void shouldDisplayPasswordRequiredMessageWhenNoPasswordProvided() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        loginPage.loginUser("fakeuser@op.pl", "");
        Assertions.assertTrue(loginPage.isPasswordRequiredAlertDisplayed(), "password required message should be displayed");
    }
}