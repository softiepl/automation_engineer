package stage5.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public LoginPage(WebDriver webDriver, WebDriverWait webDriverWait) {
        this.driver = webDriver;
        this.wait = webDriverWait;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "email")
    private WebElement emailInput;

    @FindBy(id = "passwd")
    private WebElement passwordInput;

    @FindBy(id = "SubmitLogin")
    private WebElement loginButton;

    @FindBy(id = "email_create")
    private WebElement newEmailInput;

    @FindBy(id = "SubmitCreate")
    private WebElement createAccountButton;

    @FindBy(xpath = "//div[@class='alert alert-danger']/ol")
    private WebElement authenticationAlert;

    public void loginUser(String email, String password) {
        wait.until(ExpectedConditions.elementToBeClickable(emailInput));
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        loginButton.click();
    }

    public RegisterPage createNewAccount(String email) {
        wait.until(ExpectedConditions.elementToBeClickable(newEmailInput));
        newEmailInput.sendKeys(email);
        createAccountButton.click();
        return new RegisterPage(driver, wait);
    }

    public boolean isFailedAuthenticationAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("Authentication failed.");
    }

    public boolean isPasswordRequiredAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("Password is required.");
    }
}