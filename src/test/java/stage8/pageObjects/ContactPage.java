package stage8.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactPage extends BasePage {

    private WebDriver driver;
    private WebDriverWait wait;

    public ContactPage(WebDriver webDriver, WebDriverWait webDriverWait) {
        super(webDriver, webDriverWait);
        this.driver = webDriver;
        this.wait = webDriverWait;
    }

    @FindBy(css = "h1.page-heading")
    private WebElement pageHeader;

    public boolean isDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(pageHeader));
        return pageHeader.getText().contains("CUSTOMER SERVICE - CONTACT US");
    }
}
