package stage8.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class UserDataGenerator {

    private Gender gender;
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    private String generate(){
        List<String> stringList = new ArrayList<>();
        stringList = Arrays.asList("Cat","Title","Testingapp","EryK88","Ricardo91","1212991","kto4","AutomatedTests","Computer","spaces+");
        StringBuilder randomString = new StringBuilder();

        int iterationsNumber;
        Random rand = new Random();
        iterationsNumber = rand.nextInt(2)+2;

        for(int i = 0; i < iterationsNumber; i++){
            randomString.append(stringList.get(new Random().nextInt(9)));
        }
        return randomString.toString();
    }

    private String generateEmail(){
        return generate() + "@edu.com";
    }

    private Gender generateGender(){
        Long timestamp = System.currentTimeMillis();
        if (timestamp % 2 == 0){
            return Gender.male;
        }
        else {
            return Gender.female;
        }
    }

    public UserDataGenerator(){
        gender = generateGender();
        firstName = generate();
        lastName = generate();
        email = generateEmail();
        password = generate();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Gender getGender() {
        return gender;
    }
}
