package stage8.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import stage8.pageObjects.ContactPage;
import stage8.pageObjects.LoginPage;
import stage8.pageObjects.MainPage;
import stage8.pageObjects.RegisterPage;

public class NavigationTest extends BaseTest {

    @Test
    void shouldNavigateFromMainPageToLoginPage() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        Assertions.assertTrue(loginPage.isDisplayed(), "login page should be displayed");
    }

    @Test
    void shouldNavigateFromMainPageToContactPage() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        ContactPage contactPage = mainPage.goToContactPage();
        Assertions.assertTrue(contactPage.isDisplayed(),"contact page should be displayed");
    }

    @Test
    void shouldNavigateFromLoginPageToContactPage() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        ContactPage contactPage = loginPage.goToContactPage();
        Assertions.assertTrue(contactPage.isDisplayed(),"contact page should be displayed");
    }

    @Test
    void shouldNavigateFromLoginPageToRegisterPage() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();
        RegisterPage registerPage = loginPage.createNewAccount("zbyszek@op.pl");
        Assertions.assertTrue(registerPage.isDisplayed(),"register page should be displayed");
    }
}