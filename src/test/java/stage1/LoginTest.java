package stage1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    @Test
    void shouldDisplayAuthenticationErrorWhenProvidedIncorrectCredentials() {
        String URL = "http://automationpractice.com";
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver,5);

//        open website
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(URL);

//        go to login page
        WebElement signInButton = driver.findElement(By.className("login"));
        signInButton.click();

//        try to log in with incorrect credentials
        WebElement emailInput = driver.findElement(By.id("email"));
        wait.until(ExpectedConditions.elementToBeClickable(emailInput));
        emailInput.sendKeys("fakeuser@op.pl");

        WebElement passwordInput = driver.findElement(By.id("passwd"));
        passwordInput.sendKeys("fakepassword");

        WebElement loginButton = driver.findElement(By.id("SubmitLogin"));
        loginButton.click();

//        check is "failed authentication" message is displayed
        WebElement authenticationAlert = driver.findElement(By.xpath("//div[@class='alert alert-danger']/ol"));
        Assertions.assertTrue(authenticationAlert.getText().contains("Authentication failed."), "failed authentication message should be displayed");
    }
}