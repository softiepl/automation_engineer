package stage7.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BasePage {

    private final String URL = "http://automationpractice.com";
    private WebDriver driver;
    private WebDriverWait wait;

    public BasePage(WebDriver webDriver, WebDriverWait webDriverWait) {
        this.driver = webDriver;
        this.wait = webDriverWait;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "login")
    private WebElement signInButton;

    @FindBy(xpath = "//a[text()='Contact us']")
    private WebElement contactUsButton;

    public LoginPage goToLoginPage(){
        wait.until(ExpectedConditions.elementToBeClickable(signInButton));
        signInButton.click();
        return new LoginPage(driver, wait);
    }

    public ContactPage goToContactPage(){
        wait.until(ExpectedConditions.elementToBeClickable(contactUsButton));
        contactUsButton.click();
        return new ContactPage(driver, wait);
    }

    public void open() {
        driver.get(URL);
    }
}
