package stage7.pageObjects;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage extends BasePage {

    private WebDriver driver;
    private WebDriverWait wait;

    public RegisterPage(WebDriver webDriver, WebDriverWait webDriverWait) {
        super(webDriver, webDriverWait);
        this.driver = webDriver;
        this.wait = webDriverWait;
    }

    @FindBy(css = "h1.page-heading")
    private WebElement pageHeader;

    @FindBy(xpath = "//div[@class='radio-inline'][1]")
    private WebElement mrGenderRadioButton;

    @FindBy(xpath = "//div[@class='radio-inline'][2]")
    private WebElement mrsGenderRadioButton;

    @FindBy(id = "customer_firstname")
    private WebElement firstNameInput;

    @FindBy(id = "customer_lastname")
    private WebElement lastNameInput;

    @FindBy(id = "email")
    private WebElement emailInput;

    @FindBy(id = "passwd")
    private WebElement passwordInput;

    @FindBy(xpath = "//button[span[text()='Register']]")
    private WebElement registerButton;

    @FindBy(xpath = "//div[@class='alert alert-danger']")
    private WebElement registrationFailedAlert;

    public boolean isDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(firstNameInput));
        System.out.println(pageHeader.getText());
        return pageHeader.getText().contains("CREATE AN ACCOUNT");
    }

    public void registerUser(String firstName, String lastName, String password) {
        wait.until(ExpectedConditions.visibilityOf(firstNameInput));
        firstNameInput.sendKeys(firstName);
        lastNameInput.sendKeys(lastName);
        passwordInput.sendKeys(password);
        registerButton.click();
    }

    public boolean isFailedRegistrationAlertDisplayed() {
        try{
            return registrationFailedAlert.isDisplayed();
        }
        catch (NoSuchElementException e){
            return false;
        }
    }
}
