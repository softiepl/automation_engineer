package stage7.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage extends BasePage {

    private WebDriver driver;
    private WebDriverWait wait;

    public MainPage(WebDriver webDriver, WebDriverWait webDriverWait) {
        super(webDriver, webDriverWait);
        this.driver = webDriver;
        this.wait = webDriverWait;
    }

    @FindBy(xpath = "//button[text()='Shop now !']")
    private WebElement shopNowButton;
}