package stage7.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import stage7.pageObjects.LoginPage;
import stage7.pageObjects.MainPage;
import stage7.pageObjects.RegisterPage;
import stage7.utils.UserDataGenerator;


public class RegisterTest extends BaseTest {

    @Test
    void shouldDisplayRegistrationErrorWhenMissingRequiredData() {
        MainPage mainPage = new MainPage(driver, wait);
        mainPage.open();
        LoginPage loginPage = mainPage.goToLoginPage();

        UserDataGenerator userData = new UserDataGenerator();
        RegisterPage registerPage = loginPage.createNewAccount(userData.getEmail());
        registerPage.registerUser(
                userData.getFirstName(),
                userData.getLastName(),
                userData.getPassword());
        Assertions.assertTrue(registerPage.isFailedRegistrationAlertDisplayed(), "failed registration message should be displayed");
    }
}