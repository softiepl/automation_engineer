package stage7.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class UserDataGenerator {

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    private String generateWord() {
        List<String> stringList = Arrays.asList("Cat", "EloElon", "Testingapp", "EryK88", "Ricardo91", "1212991", "kto4", "AutomatedTests", "Computer", "spaceX");
        StringBuilder randomString = new StringBuilder();

        int iterationsNumber;
        Random rand = new Random();
        iterationsNumber = rand.nextInt(2) + 1;

        for (int i = 0; i < iterationsNumber; i++) {
            randomString.append(stringList.get(new Random().nextInt(9)));
        }
        return randomString.toString();
    }

    private String generateEmail() {
        return generateWord() + "@edu.com";
    }

    public UserDataGenerator() {
        firstName = generateWord();
        lastName = generateWord();
        email = generateEmail();
        password = generateWord();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
